require lvm2.inc

SRC_URI[md5sum] = "66df72b32e1b8798dd165302e1bdd5dc"
SRC_URI[sha256sum] = "4964b93778dd37ffb7ecdc1b16820710a851fcbb2ba822e42dd4030fb1cc0642"

DEPENDS += "autoconf-archive-native"

TARGET_CC_ARCH += "${LDFLAGS}"

do_install() {
    oe_runmake 'DESTDIR=${D}' -C libdm install
    oe_runmake 'DESTDIR=${D}' -C tools install_device-mapper
}

RRECOMMENDS_${PN}_append_class-target = " lvm2-udevrules"

BBCLASSEXTEND = "native nativesdk"

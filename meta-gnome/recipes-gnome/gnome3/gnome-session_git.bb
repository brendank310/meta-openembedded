DESCRIPTION = "GNOME Session"
LICENSE = "GPLv2"
DEPENDS = " \
            adwaita-icon-theme \
            glib-2.0 \
            gconf \
            gsettings-desktop-schemas \
            gjs \
            gdm \
            gnome-desktop3 \
            mutter \
            wayland \
            wayland-protocols \
            evolution-data-server \
            libcroco \
            polkit \
            gnome-bluetooth \
            gstreamer \
            keybinder \
            gnome-settings-daemon \
            ibus \
            librsvg \
            sassc-native \
            gnome-shell \
            libxslt-native \
            xmlto-native \
            upower \
            pango \
          "

LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRC_URI = "git://gitlab.gnome.org/GNOME/gnome-session.git;protocol=http;branch=master;tag=3.30.1 \
           file://no-fail-on-missing-components.patch \
           "

RDEPENDS_gnome-session += " python3-core python3-pygobject gnome-settings-daemon gnome-bluetooth gdm librsvg-gtk "

S = "${WORKDIR}/git"

inherit pkgconfig meson gobject-introspection gettext

EXTRA_OEMESON += " -Dman=false -Dnetworkmanager=false "

do_configure_prepend() {
	# Fixup the gsettings version
	sed -i 's^3.27.90^3.24.1^g' ${S}/meson.build
	sed -i "s^@PYTHON3_PATH@^${RECIPE_SYSROOT_NATIVE}/${bindir}/python3^g" ${S}/meson.build
}

FILES_${PN} += "${datadir}/icons"
FILES_${PN} += "${datadir}/GConf"
FILES_${PN} += "${datadir}/glib-2.0"
FILES_${PN} += "${datadir}/gnome-session"
FILES_${PN} += "${datadir}/wayland-sessions"
FILES_${PN} += "${datadir}/xsessions"

do_install_append() {
	chmod +x ${D}/usr/libexec/*
}
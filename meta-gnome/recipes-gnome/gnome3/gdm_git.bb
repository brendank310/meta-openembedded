DESCRIPTION = "GNOME Desktop Manager"
LICENSE = "GPLv2"
DEPENDS = " \
           accountsservice \
           dconf-native \
           gconf-native \
           glib-2.0 \
           gtk+3 \
           libcanberra \
           plymouth \
          "

LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRC_URI = "git://gitlab.gnome.org/GNOME/gdm.git;protocol=http;branch=master;tag=3.30.1 \
           file://custom-conf.patch \
           file://root-pam-auth.patch \
           file://Xsession \
           file://install-pam-data.patch \
           "

S = "${WORKDIR}/git"

inherit autotools pkgconfig distro_features_check gobject-introspection gettext useradd systemd

PACKAGECONFIG ?= " \
     ${@bb.utils.filter('DISTRO_FEATURES', 'pam', d)} \
     plymouth \
"

PACKAGECONFIG[plymouth] = "--with-plymouth,plymouth"
PACKAGECONFIG[pam] = "--with-default-pam-config=openembedded,,libpam"

do_configure_prepend() {
	install -d ${S}/build-aux
	sed -i '/AC_CHECK_FILE/d' ${S}/configure.ac
}

do_install_append() {
	chown -R gdm:gdm ${D}${localstatedir}/lib/gdm

	# install Xsession
	install -m 0755 ${WORKDIR}/Xsession ${D}${sysconfdir}/gdm/Xsession

	# Remove installed volatiles.
	rm -rf ${D}${localstatedir}/run

	VOLATILE_DIRS=" \
		${localstatedir}/lib/gdm/.local/share/xorg \
	"

	if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
		install -d ${D}${sysconfdir}/tmpfiles.d
		for i in ${VOLATILE_DIRS}; do
			echo "d $i 0755 gdm gdm - -" >> ${D}${sysconfdir}/tmpfiles.d/gdm.conf
		done
	fi

	if ${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', 'true', 'false', d)}; then
		install -d ${D}${sysconfdir}/default/volatiles
		for i in ${VOLATILE_DIRS}; do
			echo "d gdm gdm 0755 $i none" >> ${D}${sysconfdir}/default/volatiles/99_gdm
		done
	fi

	# anyone installing gdm into their image most likely wants to run it by default
	cat >> ${D}${systemd_system_unitdir}/gdm.service <<EOF
[Install]
WantedBy=multi-user.target
EOF
}

pkg_postinst_${PN}() {
    if [ -z "$D" ]; then
        if command -v systemd-tmpfiles >/dev/null; then
            systemd-tmpfiles --create ${sysconfdir}/tmpfiles.d/gdm.conf
        elif [ -e ${sysconfdir}/init.d/populate-volatile.sh ]; then
            ${sysconfdir}/init.d/populate-volatile.sh update
        fi
    fi
}

USERADD_PACKAGES = "${PN}"
USERADD_PARAM_${PN} = " \
    --system --home /var/lib/gdm \
    --no-create-home --shell /bin/false \
    --user-group gdm \
"

SYSTEMD_SERVICE_${PN} = "gdm.service"

FILES_${PN} += "${libdir}/security/pam_gdm.so"
FILES_${PN} += "${datadir}/dconf"
FILES_${PN} += "${datadir}/glib-2.0"
FILES_${PN} += "${datadir}/gnome-session"
FILES_${PN} += "${datadir}/icons"
